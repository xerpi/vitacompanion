#include "cmd_definitions.h"
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <vitasdk.h>
#include <taihen.h>

#define COUNT_OF(arr) (sizeof(arr) / sizeof(arr[0]))

const cmd_definition cmd_definitions[] = {
    {.name = "help", .description = "Display this help screen", .arg_count = 0, .executor = &cmd_help},
    {.name = "destroy", .description = "Kill all running applications", .arg_count = 0, .executor = &cmd_destroy},
    {.name = "launch", .description = "Launch an app by Title ID", .arg_count = 1, .executor = &cmd_launch},
    {.name = "launch_1_arg", .description = "Launch an app by Title ID (with 1 argument)", .arg_count = 2, .executor = &cmd_launch_1_arg},
    {.name = "reboot", .description = "Reboot the console", .arg_count = 0, .executor = &cmd_reboot},
    {.name = "screen", .description = "Turn the screen on or off", .arg_count = 1, .executor = &cmd_screen},
    {.name = "load_skprx", .arg_count = 1, .executor = &cmd_load_skprx},
    {.name = "unload_skprx", .arg_count = 0, .executor = &cmd_unload_skprx}
};

const cmd_definition *cmd_get_definition(char *cmd_name) {
  for (unsigned int i = 0; i < COUNT_OF(cmd_definitions); i++) {
    if (!strcmp(cmd_name, cmd_definitions[i].name)) {
      return &(cmd_definitions[i]);
    }
  }

  return NULL;
}

void cmd_help(char **arg_list, size_t arg_count, char *res_msg) {
  char buf[2000] = {0};
  int longest_cmd = 0;

  for (int i = 0; i < COUNT_OF(cmd_definitions); ++i) {
    int cmd_length = strlen(cmd_definitions[i].name);

    if (cmd_length > longest_cmd) {
      longest_cmd = cmd_length;
    }
  }

  sprintf(buf, "%-*s\t\t%s\n", longest_cmd, "Command", "Description");
  strcpy(res_msg, buf);

  for (int i = 0; i < COUNT_OF(cmd_definitions); ++i) {
    sprintf(buf, "%-*s\t\t%s\n", longest_cmd, cmd_definitions[i].name, cmd_definitions[i].description);
    strcat(res_msg, buf);
  }

}

void cmd_destroy(char **arg_list, size_t arg_count, char *res_msg) {
  sceAppMgrDestroyOtherApp();
  strcpy(res_msg, "Apps destroyed.\n");
}

void cmd_launch(char **arg_list, size_t arg_count, char *res_msg) {
  char uri[32];

  snprintf(uri, 32, "psgm:play?titleid=%s", arg_list[1]);

  if (sceAppMgrLaunchAppByUri(0x20000, uri) < 0) {
    strcpy(res_msg, "Error: cannot launch the app. Is the TITLEID correct?\n");
  } else {
    strcpy(res_msg, "Launched.\n");
  }
}

void cmd_launch_1_arg(char **arg_list, size_t arg_count, char *res_msg) {
  char uri[1024];

  // sprintf(uri, "psgm:play?titleid=%s&param=%s&param2=%s", "RETROVITA", core, rom);.

  snprintf(uri, sizeof(uri), "psgm:play?titleid=%s&param=%s", arg_list[1], arg_list[2]);

  if (sceAppMgrLaunchAppByUri(0x20000, uri) < 0) {
    strcpy(res_msg, "Error: cannot launch the app. Is the TITLEID correct?\n");
  } else {
    strcpy(res_msg, "Launched.\n");
  }
}

void cmd_reboot(char **arg_list, size_t arg_count, char *res_msg) {
  scePowerRequestColdReset();
  strcpy(res_msg, "Rebooting...\n");
}

void cmd_screen(char **arg_list, size_t arg_count, char *res_msg) {
  char *state = arg_list[1];

  if (!strcmp(state, "on")) {
    scePowerRequestDisplayOn();
    strcpy(res_msg, "Turning display on...\n");
  } else if (!strcmp(state, "off")) {
    scePowerRequestDisplayOff();
    strcpy(res_msg, "Turning display off...\n");
  } else {
    strcpy(res_msg, "Error: param should be 'on' or 'off'\n");
  }
}

static SceUID mod_uid = -1;

static int unload_skprx(SceUID uid) {
  tai_module_args_t arg;
  arg.size = sizeof(arg);
  arg.pid = KERNEL_PID;
  arg.args = 0;
  arg.argp = NULL;
  arg.flags = 0;
  return taiStopUnloadKernelModuleForUser(uid, &arg, NULL, NULL);
}

void cmd_load_skprx(char **arg_list, size_t arg_count, char *res_msg) {
  char *path = arg_list[1];

  if (mod_uid != -1) {
    unload_skprx(mod_uid);
    mod_uid = -1;
  }

  tai_module_args_t arg;
  arg.size = sizeof(arg);
  arg.pid = KERNEL_PID;
  arg.args = 0;
  arg.argp = NULL;
  arg.flags = 0;
  mod_uid = taiLoadStartKernelModuleForUser(path, &arg);
}

void cmd_unload_skprx(char **arg_list, size_t arg_count, char *res_msg) {
  if (mod_uid != -1) {
    unload_skprx(mod_uid);
    mod_uid = -1;
  }
}
